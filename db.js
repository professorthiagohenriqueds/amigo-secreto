const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/web-app');

const userSchema = new mongoose.Schema({
    username: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    }
  }, { collection: 'users' }
  );
  module.exports = { Mongoose: mongoose, UserSchema: userSchema }
  