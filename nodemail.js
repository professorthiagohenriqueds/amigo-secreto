const mailer = require("nodemailer");

module.exports = (username, email) => {
    const transporter = mailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'gregoria.bartoletti83@ethereal.email',
            pass: 'uHqAtBfXvTEyeS5e99'
        }
      });
    
    const mail = {
        from: "Prof. Luiz <contato@luiztools.com.br>",
        to: "ricardo@econdos.com.br",
        subject: `Amigo secreto sorteado`,
        text: `Nome: ${username}\n email: ${email}`,
    }
    
    return new Promise((resolve, reject) => {
        transporter.sendMail(mail)
            .then(response => {
                transporter.close();
                return resolve(response);
            })
            .catch(error => {
                transporter.close();
                return reject(error);
            });
    })
}