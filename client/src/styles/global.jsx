import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        border: none;
        box-sizing: border-box;     
    }
    :root {
        font-size: 20px;
        --white: #ffffff;
        --black: #0C0D0D;
        --gray: #666360;
        --gold: #DAA520;
        --bg-flare: linear-gradient(to bottom, #f12711, #f5af19);
        --bg-zinc: linear-gradient(to bottom, #ada996, #f2f2f2, #dbdbdb, #eaeaea);
        --bg-lush: linear-gradient(to top, #bdc3c7, #2c3e50);
        --bg-royal: linear-gradient(to right, #141e30, #243b55);;
        --bg-cool-sky: linear-gradient(to bottom, #000000, #434343); 
        --bg-red: linear-gradient(to bottom, #ed213a, #93291e); 
    }

    body {
        background: var(--bg-lush);
        color: var(--black);
    }

    body, input {
        font-family: monospace;
        font-size: 1rem;
    }

    button {
        cursor: pointer;
    }
`;
