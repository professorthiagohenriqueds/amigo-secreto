import React, { createContext, useState, useEffect } from 'react';
import { api } from '../../utils/apis';

export const UsersContext = createContext();

export const UsersProvider = ({children}) => {
    const [ users, setUsers ] = useState([]);

    const getUsers = () => {
        api
            .get("/users")
            .then(res => setUsers(res.data))
            .catch(err => console.log(err));
    };

    useEffect(() => {
        getUsers()
    }, []);

    return (
        <UsersContext.Provider value={{ users, getUsers }}>
            {children}
        </UsersContext.Provider>
    )
}
