import { UsersProvider } from './UsersProvider';
import { WindowSizeProvider } from './WindowSizeProvider'

const Providers = ({children}) => {
    return (
        <WindowSizeProvider>
        <UsersProvider>
                {children}    
        </UsersProvider>
        </WindowSizeProvider>
    )
}

export default Providers;
