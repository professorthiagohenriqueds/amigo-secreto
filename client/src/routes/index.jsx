import React from 'react';
import { Route, Switch } from "react-router";

import NavMenu from '../components/NavMenu';
import DrawPage from '../pages/DrawPage';
import LandingPage from '../pages/LandingPage';
import RegisterPage from '../pages/RegisterPage';
import UpdatePage from '../pages/UpdatePage';

const Routes = () => {

    return (
    <Switch>        

        <Route exact path='/'>
            <NavMenu/>    
            <LandingPage />
        </Route>

        <Route exact path='/register'>
            <NavMenu />
            <RegisterPage />
        </Route>

        <Route exact path='/update'>
            <NavMenu />
            <UpdatePage />
        </Route>

        <Route exact path='/draw-user'>
            <NavMenu />
            <DrawPage />
        </Route>

    </Switch>
    )
}

export default Routes;
