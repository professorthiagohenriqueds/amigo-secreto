import React from 'react';
import UsersListing from '../../components/UsersListing'
import { Page } from './styles';

const LandingPage = () => {
    return (
        <Page>
            <UsersListing />
        </Page>
    )
}

export default LandingPage;