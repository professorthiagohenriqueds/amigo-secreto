import React from 'react';
import DrawButton from '../../components/DrawButton';
import { Page } from './styles';

const DrawPage = () => {
    return (
        <Page>
            <DrawButton />
        </Page>
    )
}

export default DrawPage;