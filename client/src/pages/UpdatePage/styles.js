import styled from 'styled-components';
import { TextField } from "@material-ui/core";

export const Page = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
    padding: 10px;
    max-width: 425px;
    min-height: 100vh;
    border-width: 0 8px;
    border-style: groove;
    border-color: var(--gold);
    background: var(--bg-cool-sky);
    text-align: center;
`

export const TextFieldStyled = styled(TextField)`
  && {
    div {
        background-color: var(--white);
        font-size: 0.8rem;
    }
  }
`;
