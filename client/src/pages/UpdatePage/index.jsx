import React, { useState } from 'react';
import UserUpdate from '../../components/UserUpdate';
import DeleteButton from '../../components/DeleteButton'
import InputAdornment from '@material-ui/core/InputAdornment';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { Page, TextFieldStyled } from './styles';

const UpdatePage = () => {
    const [ id, setId ] = useState("")

    return (
    <Page>
        <TextFieldStyled
            onChange={(event) => setId(event.target.value)} 
            label="Id do usuário"
            InputProps={{
                startAdornment: (
                <InputAdornment position="start">
                    <VpnKeyIcon />
                </InputAdornment>
                ),
            }}
            autoComplete="off"
            variant="filled"
            color = "primary"
            size="big"
        />
        <UserUpdate id={id}/>
        <DeleteButton id={id}/>
    </Page>
    )
}

export default UpdatePage;