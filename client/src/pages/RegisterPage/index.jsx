import React from 'react';
import RegisterUser from '../../components/UserRegister';
import { Page } from './styles';

const RegisterPage = () => {
    return (
        <Page>
            <RegisterUser />
        </Page>
    )
}

export default RegisterPage;