import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    overflow-y: hidden;
    max-height: 80vh;
    min-width: 300px;
    margin-top: 80px;
    background: var(--bg-zinc);
    padding: 5px;
    align-items: center;
    color: var(--white);
    justify-content: space-between;
    border: 3px double var(--gray);

    h1{
        font-size: 2rem;
        text-align: center;
        color: var(--black);
    }

`
