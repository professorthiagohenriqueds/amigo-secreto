import React, { useContext } from 'react';
import { Container } from './styles';
import UserCard from '../UserCard';
import { UsersContext } from '../../Providers/UsersProvider';

const DrinkList = () => {
    const { users } = useContext(UsersContext)

    return (
        <Container>
        {
            users.length > 0 
            ? users.map((user, index) => <UserCard key={index} user={user}/>)
            : <h1>Usuários não encontrados!</h1>
        }
        </Container>
    );
}

export default DrinkList;
