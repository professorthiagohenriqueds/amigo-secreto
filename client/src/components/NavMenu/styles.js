import styled from 'styled-components';

export const Nav = styled.nav`
    position:absolute;
    top: 0;
    display: flex;
    font-size: 0.7rem;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    background: var(--bg-lush);
    width: 100%;
    min-height: 60px;
    padding: 5px 15px;

    a {
        color: var(--white);
    }
    a:hover{
        color: var(--white);
    }

    @media (min-width: 800px){
        flex-direction: row;
    }
`
