import React, {useState, useContext} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom'
import {Nav} from './styles'
import { WindowSizeContext } from '../../Providers/WindowSizeProvider';


const NavMenu = () => {
  const { useWindowDimensions } = useContext(WindowSizeContext)
  const [anchorEl, setAnchorEl] = useState(null);

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const [ size ] = useState(useWindowDimensions()) 

  return (
    <>
    {size.width > 768 
      ?      
      <Nav>
          <Link to='/'><h2>Usuários</h2></Link>
          <Link to='/register'><h2>Cadastro</h2></Link>
          <Link to="/update"><h2>Atualização</h2></Link>
          <Link to='/draw-user'><h2>Sorteio</h2></Link>
      </Nav>
      :
      <Nav>
      <Button
        id="basic-button"
        aria-controls="basic-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        Menu
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={handleClose}><Link to='/'><h2>Usuários</h2></Link></MenuItem>
        <MenuItem onClick={handleClose}><Link to='/register'><h2>Cadastro</h2></Link></MenuItem>
        <MenuItem onClick={handleClose}><Link to="/update"><h2>Atualização</h2></Link></MenuItem>
        <MenuItem onClick={handleClose}><Link to='/draw-user'><h2>Sorteio</h2></Link></MenuItem>
      </Menu>
    </Nav>
    }

    </>
  );
}

export default NavMenu;