import styled from 'styled-components';

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    border: 2px solid var(--white);
    color: var(--white);
    margin: 10px auto;
    width: 240px;
    height: 80px;
    padding: 5px;
    background: var(--black);

    h2 {
        font-size: 1rem;
        text-align: center;
    }

    p{
        text-align: justify;
        margin: 5px 0;
        font-size: 0.5rem;
    }
`
