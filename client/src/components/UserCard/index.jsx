import React from 'react';
import { Card } from './styles';

const UserCard = ({ user }) => {
    const { _id, username, email } = user;

    return (
    <Card>
        <h2>{username}</h2>
        <p><b>ID:</b> {_id}</p>
        <p>{email}</p>
    </Card>
    );
}

export default UserCard;
