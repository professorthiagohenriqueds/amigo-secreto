import styled from 'styled-components';
import { TextField } from "@material-ui/core";

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 25px 0;
    width: 90%;
    padding: 10px;
    text-align: center;
    border-width: 10px 0;
    border-style: double;
    border-color: var(--gray);

    >div {
        background-color: var(--white);
        width: 100%;
        margin-top: 20px;
    }
`
export const Btn = styled.button`
    width: 200px;
    height: 2.2rem;
    color: var(--white);
    border-radius: 50px;
    border: 3px inset var(--gray);
    font-weight: bold;
    background: var(--black);
    font-size: 1rem;
    margin: 20px 0;
    position: relative;
    font-family: 'Bowlby One SC', cursive;
    
    &:hover, &:focus{
        transition: 1.5s;
        background: var(--bg-royal);
        border-style: outset;
    }

`

export const TextFieldStyled = styled(TextField)`
  && {
    * {
      font-size: 0.8rem;
    }
  }
`;
