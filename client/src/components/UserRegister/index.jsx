import React, { useContext } from 'react';
import { api } from '../../utils/apis';
import { Form, Btn, TextFieldStyled } from './styles'
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';
import EmailIcon from '@material-ui/icons/Email';
import { UsersContext } from '../../Providers/UsersProvider';

import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";

import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";

const RegisterUser = () => {
  const { getUsers } = useContext(UsersContext)

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório!"),
    email: yup.string().email("Email inválido!").required("Campo obrigatório!"),
  });

  const { 
    register, 
    handleSubmit, 
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmitFunction = ({ username, email }) => {
    const user = {username, email};
    api
      .post("/users", user)
      .then((res) => {
        console.log(res.data)
        toast.success("Usuário Criado!");    
        
        getUsers()
      })
      .catch((error) => {
        let errorMessage = "Ocorreu algum erro ao criar o usuário!"

        if(error.response.status === 409){
          errorMessage = "Já existe um usuário com este email!"
        }
        
        toast.error(errorMessage);
      });


  };

  return (
  <Form onSubmit={handleSubmit(onSubmitFunction)}>
    <TextFieldStyled
      {...register("username")}
      error={errors.username?.message}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <AccountCircle />
          </InputAdornment>
        ),
      }}
      autoComplete="off"
      label="Nome do Usuário"
      variant="filled"
    />
    {errors.username?.message}

    <TextFieldStyled
      {...register("email")}
      error={errors.email?.message}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <EmailIcon />
          </InputAdornment>
        ),
      }}
      autoComplete="off"
      label="Email do usuário"
      variant="filled"
    />
    {errors.email?.message}


    <Btn type='submit'>Cadastrar</Btn>

  </Form>
  )
}

export default RegisterUser;