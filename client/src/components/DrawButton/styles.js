import styled from 'styled-components';

export const Button = styled.button`
    width: 240px;
    height: 240px;
    border-radius: 50%;
    font-weight: bold;
    background: var(--bg-red);
    font-size: 1.4rem;
    margin: 15px auto;
    position: relative;
    border: 6px inset var(--white);
    outline-offset: 10px;
    outline: 5px double var(--white);
    font-family: 'Shojumaru', cursive;
    
    &:hover, &:focus{
        transition: 1s;
        background: var(--bg-flare);
        border-style: outset;
        color: var(--white);
    }

`