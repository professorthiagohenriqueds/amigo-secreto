import React, { useContext } from 'react';
import { api } from '../../utils/apis';
import { Button } from './styles.js'

import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import { UsersContext } from '../../Providers/UsersProvider';

const DrawButton = () => {
  const { users } = useContext(UsersContext)

  const sendEmail = (user) => {
      api
        .post('/send', user)
        .then(res => {
          console.log(res.data)
          toast.success("Email enviado com sucesso!")
        })
        .catch((err) => {
          toast.error("Erro ao enviar o Email!")
          console.log(err)
        })      
  }

  const randomNumber = (number) => {
    return Math.ceil(Math.random()*(number));
  }

  const getUser = () => {
    let draw = randomNumber(users.length);

    return users[draw]
  }

  const onClick = async() =>{
    const friend = getUser()

    if (friend === null){
      toast.error("Não foram encontrados usuários!")
    } else {
      sendEmail(friend)
    }

  }

  return (
    <Button onClick={onClick}>Sorteio</Button>
  )
}

export default DrawButton;