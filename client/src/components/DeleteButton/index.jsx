import React, { useContext } from 'react';
import { api } from '../../utils/apis';
import { Button } from './styles.js'

import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import { UsersContext } from '../../Providers/UsersProvider';

const DeleteButton = ({ id }) => {
  const { getUsers } = useContext(UsersContext)

  const onClick = () =>{
    api
        .delete(`/users/${id}`)
        .then(res => {
            console.log(res.data)
            toast.success("Usuário deletado com sucesso!")    
            
            getUsers()
        })
        .catch((err) => {
          let errorMessage = "Erro ao deletar usuário!"

          if (err.response.status === 404){
            errorMessage = "Usuário com id não encontrado!"
          } else if (err.response.status === 400){
            errorMessage = "Id em formato incorreto!"
          } 
          
          toast.error(errorMessage)
        })
    

  }

  return (
    <Button onClick={onClick}>Deletar</Button>
  )
}

export default DeleteButton;