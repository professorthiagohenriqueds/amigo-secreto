import styled from 'styled-components';

export const Button = styled.button`
    width: 80%;
    height: 120x;
    border-radius: 40px;
    font-weight: bold;
    background: var(--bg-red);
    font-size: 1.4rem;
    margin: 15px auto;
    position: relative;
    border: 2px inset var(--gray);
    font-family: 'Bowlby One SC', cursive;
    
    &:hover, &:focus{
        transition: 1s;
        background: var(--bg-flare);
        border-style: outset;
        color: var(--white);
    }

`