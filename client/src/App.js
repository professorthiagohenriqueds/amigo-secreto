import React from 'react';
import { ToastContainer } from "react-toastify";
import { GlobalStyle } from "./styles/global.jsx";
import Routes from "./routes";

function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <ToastContainer 
        position="bottom-center"
      />      
      <Routes />
    </div>
  );
}

export default App;

