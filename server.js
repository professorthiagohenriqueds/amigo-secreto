const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));
app.use(express.json()) // for parsing application/json

/* GET home page. */
app.get('/users', async (req, res) => {
  const db = require("./db");
  const Users = db.Mongoose.model('users', db.UserSchema, 'users');

  const docs = await Users.find({}).lean().exec();
  
  res.send(docs);
});

app.post('/send', (req, res) => { 
  const email = req.body.email
  const username = req.body.username

  require("./nodemail")(username, email)
      .then(response => res.json(response))
      .catch(error => res.json(error));
})


app.patch('/users/:id', async (req, res) => {
  const db = require("./db");
  const Users = db.Mongoose.model('users', db.UserSchema, 'users');  
  const user_id = req.params.id;
  const user = req.body;

  if(user_id.length < 24){
    res.sendStatus(400);
  } else {
    try{
      await Users.findByIdAndUpdate(user_id, user, {runValidators: true})

      const updatedUser = await Users.findById(user_id).lean().exec()
      
      if (updatedUser === null) {
        res.sendStatus(404);
      } else {
        res.send(updatedUser); 
      }        
    } catch (error){
      res.sendStatus(409)
    }
  }
})


app.delete('/users/:id', async (req, res) => {
  const db = require("./db");
  const Users = db.Mongoose.model('users', db.UserSchema, 'users');
  const user_id = req.params.id

  if(user_id.length < 24){
    res.sendStatus(400);
  } else {
    const DeletedUser = await Users.findByIdAndDelete(user_id).lean().exec()

    if (DeletedUser === null){
      res.sendStatus(404);
    } else {
      res.send(DeletedUser);    
    }
  }
})

// for parsing application/json
app.post('/users', async (req, res) => {
  const username = req.body.username
  const email = req.body.email

  const db = require("./db");
  const Users = db.Mongoose.model('users', db.UserSchema, 'users');

  const User = new Users({username, email});

  try{
    let newUser = await User.save();

    res.send(newUser)
  } catch (error){
    res.sendStatus(409);
  }

});
